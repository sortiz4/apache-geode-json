#!/usr/bin/env python
import sys

SIZE = 2**16

if __name__ == '__main__':
    # This script will generate a large JSON file
    with open(sys.argv[1], 'w') as file:
        file.write('{')
        for i in range(SIZE):
            file.write('"{}":"{}"{}'.format(i, i, '' if i == SIZE - 1 else ','))
        file.write('}')
