#!/usr/bin/env python
import subprocess
import sys

if __name__ == "__main__":
    subprocess.run(['java', '-jar', 'build/libs/geode-0.2.0.jar', *sys.argv[1:]])
