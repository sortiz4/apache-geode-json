import java.io.BufferedReader;
import java.io.FileReader;
import java.util.Iterator;
import org.apache.geode.cache.query.internal.json.JsonDocument;
import org.apache.geode.cache.query.internal.json.JsonException;

public class Main {
    public static int ESUCCESS = 0x00;
    public static int EUSAGE = 0x01;
    public static int EPARSE = 0x02;

    public static void main(String[] args) {
        if (args.length != 1) {
            System.err.println("Usage: geode path");
            System.exit(EUSAGE);
        }

        JsonDocument document = null;
        long start = System.nanoTime();
        try {
            document = JsonDocument.load(args[0]);
        } catch (JsonException exc) {
            System.err.println(exc.getMessage());
            System.exit(EPARSE);
        }

        long end = System.nanoTime();
        System.out.println(String.format("%f ms", (double)(end - start) * 1e-6));
        System.out.println(document.getSummary());
        System.exit(ESUCCESS);
    }
}
