/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.apache.geode.cache.query.internal.json;

import com.fasterxml.jackson.databind.JsonNode;
import java.math.BigDecimal;
import java.math.BigInteger;
import org.apache.geode.cache.query.internal.json.JsonException;
import org.apache.geode.cache.query.internal.json.JsonType;

public class JsonPrimitive extends JsonType {
    private static final String ECONVERT = "cannot to convert '%s' to '%s'";
    private Object value;

    public JsonPrimitive(JsonNode node) throws JsonException {
        if (node.isTextual()) {
            this.value = node.asText();
        } else if (node.isNumber()) {
            if (node.isLong()) {
                this.value = node.asLong();
            } else if (node.isBigInteger()) {
                this.value = node.bigIntegerValue();
            } else if (node.isDouble()) {
                this.value = node.asDouble();
            } else if (node.isBigDecimal()) {
                this.value = node.decimalValue();
            } else {
                throw new JsonException("node is not a number");
            }
        } else if (node.isBoolean()) {
            this.value = node.asBoolean();
        } else if (node.isNull()) {
            this.value = null;
        } else {
            throw new JsonException("node is not a primitive");
        }
    }

    public String getSummary() {
        String type = null;
        if (this.value != null) {
            type = this.value.getClass().getSimpleName();
        } else {
            type =  "Null";
        }
        return String.format("JsonPrimitive of type %s", type);
    }

    public Object getValue() {
        return this.value;
    }

    public boolean isNull() {
        return this.value == null;
    }

    public boolean isNumber() {
        return (
            this.value instanceof Byte ||
            this.value instanceof Short ||
            this.value instanceof Integer ||
            this.value instanceof Long ||
            this.value instanceof Float ||
            this.value instanceof Double ||
            this.value instanceof BigInteger ||
            this.value instanceof BigDecimal
        );
    }

    public boolean toBoolean() throws JsonException {
        if (this.value instanceof Boolean) {
            return (Boolean)this.value;
        }
        try {
            return Boolean.parseBoolean(this.value.toString());
        } catch (NumberFormatException exc) {
            String msg = String.format(ECONVERT, this.value.getClass().getName(), Boolean.class.getName());
            throw new JsonException(msg, exc);
        }
    }

    public byte toByte() throws JsonException {
        if (this.value instanceof Byte) {
            return (Byte)this.value;
        }
        try {
            return Byte.parseByte(this.value.toString());
        } catch (NumberFormatException exc) {
            String msg = String.format(ECONVERT, this.value.getClass().getName(), Byte.class.getName());
            throw new JsonException(msg, exc);
        }
    }

    public short toShort() throws JsonException {
        if (this.value instanceof Short) {
            return (Short)this.value;
        }
        try {
            return Short.parseShort(this.value.toString());
        } catch (NumberFormatException exc) {
            String msg = String.format(ECONVERT, this.value.getClass().getName(), Short.class.getName());
            throw new JsonException(msg, exc);
        }
    }

    public int toInt() throws JsonException {
        if (this.value instanceof Integer) {
            return (Integer)this.value;
        }
        try {
            return Integer.parseInt(this.value.toString());
        } catch (NumberFormatException exc) {
            String msg = String.format(ECONVERT, this.value.getClass().getName(), Integer.class.getName());
            throw new JsonException(msg, exc);
        }
    }

    public long toLong() throws JsonException {
        if (this.value instanceof Long) {
            return (Long)this.value;
        }
        try {
            return Long.parseLong(this.value.toString());
        } catch (NumberFormatException exc) {
            String msg = String.format(ECONVERT, this.value.getClass().getName(), Long.class.getName());
            throw new JsonException(msg, exc);
        }
    }

    public float toFloat() throws JsonException {
        if (this.value instanceof Float) {
            return (Float)this.value;
        }
        try {
            return Float.parseFloat(this.value.toString());
        } catch (NumberFormatException exc) {
            String msg = String.format(ECONVERT, this.value.getClass().getName(), Float.class.getName());
            throw new JsonException(msg, exc);
        }
    }

    public double toDouble() throws JsonException {
        if (this.value instanceof Double) {
            return (Double)this.value;
        }
        try {
            return Double.parseDouble(this.value.toString());
        } catch (NumberFormatException exc) {
            String msg = String.format(ECONVERT, this.value.getClass().getName(), Double.class.getName());
            throw new JsonException(msg, exc);
        }
    }

    public BigInteger toBigInteger() throws JsonException {
        if (this.value instanceof BigInteger) {
            return (BigInteger)this.value;
        }
        try {
            return new BigInteger(this.value.toString());
        } catch (NumberFormatException exc) {
            String msg = String.format(ECONVERT, this.value.getClass().getName(), BigInteger.class.getName());
            throw new JsonException(msg, exc);
        }
    }

    public BigDecimal toBigDecimal() throws JsonException {
        if (this.value instanceof BigDecimal) {
            return (BigDecimal)this.value;
        }
        try {
            return new BigDecimal(this.value.toString());
        } catch (NumberFormatException exc) {
            String msg = String.format(ECONVERT, this.value.getClass().getName(), BigDecimal.class.getName());
            throw new JsonException(msg, exc);
        }
    }

    public String toString() {
        if (this.value == null) {
            return "null";
        } else if (this.value instanceof String) {
            return String.format("\"%s\"", this.value);
        }
        return this.value.toString();
    }
}
