/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.apache.geode.cache.query.internal.json;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import org.apache.geode.cache.query.internal.json.JsonArray;
import org.apache.geode.cache.query.internal.json.JsonException;
import org.apache.geode.cache.query.internal.json.JsonObject;
import org.apache.geode.cache.query.internal.json.JsonPrimitive;
import org.apache.geode.cache.query.internal.json.JsonType;
import org.apache.geode.cache.query.internal.json.Summarizable;

public class JsonDocument implements Summarizable {
    private JsonType value;
    private String absPath;
    private String path;

    public JsonDocument(String path, JsonType value) {
        this.path = path;
        this.value = value;
        this.absPath = null;
    }

    public static JsonDocument load(String path) throws JsonException {
        BufferedReader reader = JsonDocument.open(path);
        ObjectMapper mapper = new ObjectMapper();
        JsonNode node = null;

        // Parse the document
        try {
            node = mapper.readTree(reader);
        } catch (IOException exc) {
            String msg = String.format("'%s': could not be parsed", path);
            throw new JsonException(msg);
        } finally {
            JsonDocument.close(reader);
        }

        // Interpret the document as a JSON object
        try {
            return new JsonDocument(path, new JsonObject(node));
        } catch (JsonException exc) {
            // This branch is empty
        }

        // Interpret the document as a JSON array
        try {
            return new JsonDocument(path, new JsonArray(node));
        } catch (JsonException exc) {
            // This branch is empty
        }

        // Interpret the document as a JSON primitive
        try {
            return new JsonDocument(path, new JsonPrimitive(node));
        } catch (JsonException exc) {
            // This branch is empty
        }

        // The document is not an object or an array
        String msg = String.format("'%s': is not a valid document", path);
        throw new JsonException(msg);
    }

    public String getAbsolutePath() {
        // Memoize the absolute path
        if (this.absPath == null) {
            File file = new File(this.path);
            try {
                this.absPath = file.getAbsolutePath();
            } catch (SecurityException exc) {
                this.absPath = this.path;
            }
        }
        return this.absPath;
    }

    public String getSummary() {
        return this.value.getSummary();
    }

    public String toString() {
        return this.value.toString();
    }

    private static BufferedReader open(String path) throws JsonException {
        FileReader reader = null;
        try {
            reader = new FileReader(path);
        } catch (FileNotFoundException exc) {
            String msg = String.format("'%s': could not be opened", path);
            throw new JsonException(msg, exc);
        }
        return new BufferedReader(reader);
    }

    private static void close(BufferedReader reader) throws JsonException {
        try {
            reader.close();
        } catch (IOException exc) {
            String msg = String.format("could not close resource");
            throw new JsonException(msg, exc);
        }
    }
}
